const numA = 2
const numB = Math.pow(`${numA}`, 3);
console.log(`The cube of ${numA} is ${numB}`);

const fullAddress =  [`258 Washington Ave NW`, `California`, 90011];
const [street, state, zipCode] = fullAddress;
console.log(`I live at ${street}, ${state} ${zipCode}`)

const crocodile = {
	type: `saltwater crocodile`,
	weight: `1075 kgs`,
	length: `20 ft 3in`
}
const {type, weight, length} = crocodile;
console.log(`Lolong was a ${type}. He weighed at ${weight} with a measurement of ${length}.`)

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const petDog = new Dog(`John`, 4, `Siberian Husky`);
console.log(petDog);